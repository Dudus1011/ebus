from django.views.generic import TemplateView
from django.http import JsonResponse
from route_search.modules.algorithm_parts.algorithm import algorithm
from route_search.modules.algorithm_parts.prepare_response import prepare_response
from route_search.modules.algorithm_parts.convert_datetime import convert_datetime
from django.views import View
import json


class BaseView(TemplateView):
    '''
    Simple view which map and searching engine. In this view we can search for bus route and find the best bus
    that we want to use.
    Also in future #TODO there will be a schedule
    '''

    template_name = 'base_view/index.html'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

class FindRouteView(View):
    def post(self, request, *args, **kwargs):
        departure_day, departure_time = convert_datetime(request.POST.get('datetime'))
        results = algorithm(request.POST.get('start_location'), request.POST.get('goal_location'), departure_day , departure_time)

        response = prepare_response(results)

        request.session['last_response'] = response

        return JsonResponse(response)

class GetCoordsView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        solution_id = data.get('solution_id')

        last_response = request.session.get('last_response')

        return JsonResponse({'start': 'start'})
