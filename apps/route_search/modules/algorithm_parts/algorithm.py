from .nearest_stops import get_nearest_stops
from .common_bus_lines import common_bus_lines
from route_search.models import BusDeparture, BusStopInBusLine
from .categorize_date import categorize_date
from datetime import datetime

def algorithm(start_location, goal_location, date, time):
    day_type = categorize_date(date)
    solutions = []

    nearest_start_location_stops = get_nearest_stops(start_location)
    nearest_goal_location_stops = get_nearest_stops(goal_location)

    common_bus_lines_solution = common_bus_lines(nearest_start_location_stops, nearest_goal_location_stops)

    solutions_without_changing_bus = find_route_without_changing_bus(common_bus_lines_solution, day_type, time)

    used_buses_ids = []
    for solutions_tmp in solutions_without_changing_bus:
        for solution in solutions_tmp:
            used_buses_ids.append(solution['first_stop_departure'].bus_line.id)

    solutions_with_changing_bus = find_route_with_changing_bus(used_buses_ids, nearest_start_location_stops, nearest_goal_location_stops, day_type, time)

    solutions.extend(solutions_without_changing_bus)
    solutions.extend(solutions_with_changing_bus)
    return solutions

def find_route_without_changing_bus(common_bus_lines_solution, day_type, time):
    solutions = []
    for solution_dict in common_bus_lines_solution:
        try:
            first_stop_departure = BusDeparture.objects.filter(bus_line=solution_dict['bus_line'].id , bus_stop=solution_dict['start_stop'].id, time__gte=time, route_day=day_type).order_by('time').first()
            goal_stop_arrival = BusDeparture.objects.filter(bus_line=solution_dict['bus_line'].id , bus_stop=solution_dict['goal_stop'].id, departure_ordinal_number=first_stop_departure.departure_ordinal_number, route_day=day_type).order_by('time').first()
            solutions.append([{
                'first_stop_departure': first_stop_departure,
                'second_stop_arrival': goal_stop_arrival
            }])
        except:
            pass
    return solutions

def find_route_with_changing_bus(used_buses_ids, nearest_start_location_stops, nearest_goal_location_stops, day_type, time):
    solutions = []

    goal_stops_buses_dict = {
        stop.id: set(stop.get_buses_driving_away_from_stop(used_buses_ids)) for stop in nearest_goal_location_stops
    }
    goal_location_buses = {bus for buses in goal_stops_buses_dict.values() for bus in buses}

    all_stops_in_lines = BusStopInBusLine.objects.all().select_related('bus_stop', 'bus_line')

    for first_stop in nearest_start_location_stops:
        buses = first_stop.get_buses_driving_away_from_stop(used_buses_ids)

        for first_stop_bus in buses:
            first_stop_bus_all_stops = all_stops_in_lines.filter(bus_line=first_stop_bus.id)

            for second_stop_in_bus_line in first_stop_bus_all_stops:
                second_stop_in_first_bus = second_stop_in_bus_line.bus_stop
                second_stop_buses = second_stop_in_first_bus.get_buses_driving_away_from_stop(used_buses_ids)

                for second_stop_bus in second_stop_buses:
                    if second_stop_bus in goal_location_buses:
                        for goal_stop_id, goal_stop_buses in goal_stops_buses_dict.items():
                            if second_stop_bus in goal_stop_buses:
                                first_stop_in_second_bus = all_stops_in_lines.filter(bus_line=second_stop_bus.id, bus_stop=second_stop_in_first_bus.id)[0]
                                goal_stop_bus_stops_in_bus_line = all_stops_in_lines.filter(bus_line=second_stop_bus.id, bus_stop=goal_stop_id)
                                for goal_stop_in_bus_line in goal_stop_bus_stops_in_bus_line:
                                    if first_stop_in_second_bus.ordinal_number < goal_stop_in_bus_line.ordinal_number:
                                        used_buses_ids.append(second_stop_bus.id)
                                        try:
                                            first_stop_departure = BusDeparture.objects.filter(bus_stop=first_stop.id, bus_line=first_stop_bus.id, time__gte=time, route_day=day_type).order_by('time').first()
                                            second_stop_arrival = BusDeparture.objects.filter(bus_stop=second_stop_in_first_bus.id, bus_line=first_stop_bus.id, departure_ordinal_number=first_stop_departure.departure_ordinal_number, route_day=day_type).order_by('time').first()
                                            second_stop_departure = BusDeparture.objects.filter(bus_stop=first_stop_in_second_bus.id, bus_line=second_stop_bus.id, time__gte=second_stop_arrival.time, route_day=day_type).order_by('time').first()
                                            goal_stop_arrival = BusDeparture.objects.filter(bus_stop=goal_stop_id, bus_line=second_stop_bus.id, departure_ordinal_number=second_stop_departure.departure_ordinal_number, route_day=day_type).order_by('time').first()
                                        except:
                                            first_stop_departure = None
                                            second_stop_departure = None
                                            second_stop_arrival = None
                                            goal_stop_arrival = None
                                        if first_stop_departure and second_stop_arrival and second_stop_departure and goal_stop_arrival:
                                            part_solutions = []
                                            part_solutions.append({
                                                'first_stop_departure': first_stop_departure,
                                                'second_stop_arrival': second_stop_arrival
                                            })
                                            part_solutions.append({
                                                'first_stop_departure': second_stop_departure,
                                                'second_stop_arrival': goal_stop_arrival
                                            })
                                            solutions.append(part_solutions)
    return solutions



