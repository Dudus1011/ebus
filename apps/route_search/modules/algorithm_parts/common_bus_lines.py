from route_search.models import BusStopInBusLine

def common_bus_lines(start_stops, goal_stops):
    solution = []
    used_buses = set()

    # Pre-compute buses for each stop to avoid redundant computations
    start_stops_buses = {stop: set(stop.get_buses_driving_away_from_stop()) for stop in start_stops}
    goal_stops_buses = {stop: set(stop.get_buses_driving_away_from_stop()) for stop in goal_stops}

    for start_stop, start_stop_buses in start_stops_buses.items():
        for goal_stop, goal_stop_buses in goal_stops_buses.items():
            # Find common buses between start and goal locations
            common_buses = start_stop_buses.intersection(goal_stop_buses)

            for bus_line in common_buses:
                if bus_line not in used_buses:
                    # We assume each bus line is unique and only need to check once if it can be part of the solution
                    start_stop_in_bus_lines = BusStopInBusLine.objects.filter(bus_line=bus_line, bus_stop=start_stop)
                    goal_stop_in_bus_lines = BusStopInBusLine.objects.filter(bus_line=bus_line, bus_stop=goal_stop)

                    for start_stop_in_bus_line in start_stop_in_bus_lines:
                        for goal_stop_in_bus_line in goal_stop_in_bus_lines:
                            if start_stop_in_bus_line.ordinal_number < goal_stop_in_bus_line.ordinal_number:
                                solution.append({'bus_line': bus_line, 'start_stop': start_stop, 'goal_stop':goal_stop})
                                used_buses.add(bus_line)
                                break

    return solution