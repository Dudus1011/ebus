import holidays

pl_holidays = holidays.Poland()

def categorize_date(date):
    if date in pl_holidays:
        return 'Niedziela i święta'

    weekday = date.weekday()
    if weekday < 5:
        return 'Roboczy'
    elif weekday == 5:
        return 'Sobota'
    else:
        return 'Niedziela i święta'