def prepare_response(algorithm_results):
    response = {}

    for index, solution in enumerate(algorithm_results):  # Use enumerate to get an index
        first_bus_name = solution[0]['first_stop_departure'].bus_line.name
        first_bus_direction = solution[0]['first_stop_departure'].bus_line.direction
        time = solution[0]['first_stop_departure'].time



        prepared_solution = {
            'div': f'<div style="display: none"></div><div id="{index}" class="solution" style="width: 99%; border: solid 1px white; padding-y: 20px; border-radius: 5px; margin-bottom: 10px;"> { first_bus_name } { first_bus_direction } { time } </div>',
            #'first_stop_departure': solution[0]['first_stop_departure'].id,
            #'second_stop_arrival': solution[0]['second_stop_arrival'].id if solution.get('second_stop_arrival') else None,
            #'second_stop_departure': solution[0]['second_stop_departure'].id if solution.get('second_stop_departure') else None,
            #'goal_stop_arrival': solution[0]['goal_stop_arrival'].id
        }

        response[str(index)] = prepared_solution  # Convert index to string if needed

    return response