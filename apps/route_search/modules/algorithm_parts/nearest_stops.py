from route_search.models import BusStop
from geopy.geocoders import Nominatim

# Init of geocoder
geolocator = Nominatim(user_agent="ebus")

def get_nearest_stops(location):
    coded_location = geolocator.geocode(location + ',powiat ostrowski, województwo wielkopolskie, Polska')
    nearest_start_stops = BusStop.get_nearest_stops(5, coded_location.latitude, coded_location.longitude)

    return nearest_start_stops