from datetime import datetime


def convert_datetime(datetime_str):
    # Parse the input string into a datetime object
    dt = datetime.strptime(datetime_str, '%Y-%m-%dT%H:%M')

    # Extract the date and time from the datetime object
    date_obj = dt.date()
    time_obj = dt.time()

    return date_obj, time_obj