import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin
import re
import datetime


def generate_buses_models_data():
    base_url = 'http://rozklad.com/maps/index.php?IDKlienta=OSTROW_MZK&cmd=linie'

    response = requests.get(base_url)

    if response.status_code == 200:
        bus_lines = []
        departures = []
        soup = BeautifulSoup(response.text, 'html.parser')
        #here we find all the bus_line__name
        found_bus_lines = soup.find_all('a', class_='btn btn-outline-primary')

        for bus_line in found_bus_lines:
            href = bus_line.get('href')

            if href:
                full_url = urljoin(base_url, href)
                detail_response = requests.get(full_url)

                if detail_response.status_code == 200:
                    detail_soup = BeautifulSoup(detail_response.text, 'html.parser')
                    #here we find bus_line__directions (because there are 2 directions)
                    first_stops_column = detail_soup.find('div', id='przystanki1')
                    second_stops_column = detail_soup.find('div', id='przystanki2')

                    for stops_column in [first_stops_column, second_stops_column]:
                        direction_button = stops_column.find('button', class_='p-2 fw-bold fs-4 accordion-button')
                        if not direction_button:
                            direction_button = stops_column.find('button', class_='bg-light bg-gradient p-2 fw-bold fs-4 accordion-button')
                        direction_text = direction_button.text.strip()
                        match = re.search(r"Linia: (\S+?)Kierunek: (.+)", direction_text)
                        if match:
                            name, direction = match.groups()
                            stops = stops_column.find_all('a', class_='p-1 list-group-item list-group-item-action')

                            stops_info = [stop['id'] for stop in stops]
                            bus_lines.append({'name': name.replace('_', ''), 'direction': direction,
                                              'description': 'Podstawowy model pobrany ze strony', 'stops': stops_info})

                            for stop_id in stops_info:
                                departure_times_url = f'http://rozklad.com/maps/r7xp.php?IDKlienta=OSTROW_MZK&cmd=rozID&ID={stop_id}&IDLinii={name}'
                                departure_times_response = requests.get(departure_times_url)
                                if departure_times_response.status_code == 200:
                                    detail_soup = BeautifulSoup(departure_times_response.text, 'html.parser')
                                    departure_columns = detail_soup.find_all('div', class_='col-sm p-0 mb-1')
                                    for departure_column in departure_columns:
                                        departure_days = departure_column.find_all('button', id=['btn1', 'btn2', 'btn3'])
                                        for dep_day in departure_days:
                                            print(name, dep_day.text, stop_id)
                                            times = departure_column.find_all('li', class_='odjazd-list list-group-item')
                                            departure_ordinal_number = 1
                                            for row in times:
                                                text = row.text.strip()
                                                hour = text[:2]
                                                rest = text[2:].split('.')
                                                for minutes_row in rest:
                                                    if len(minutes_row) > 1:
                                                        minutes = minutes_row.strip()[:2]
                                                        #type = minutes_row.strip()[2:]
                                                        departures.append({'departure_ordinal_number': departure_ordinal_number, 'bus_stop_id': stop_id, 'bus_line_name': name.replace('_', ''), 'direction': direction, 'day_type': dep_day.text.strip(), 'hour': hour, 'minutes': minutes.split()[0]})
                                                        departure_ordinal_number += 1
                else:
                    print(f'Error while connecting {full_url}')
        return [departures, bus_lines]

    else:
        print('Error while connecting.')

#generate_buses_models_data()