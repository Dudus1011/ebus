'''
A little bit words from where the files are:

    Data2Json_stops:

        FILES TAKEN FROM http://rozklad.com/maps/index.php?IDKlienta=OSTROW_MZK&cmd=przystanki
        INSIDE SOURCE FILES THERE IS FILE DATA2JSON AND INSIDE THIS FILE IS A LIST OF STATIONS.

    stops_lat_lng:

        From downloaded files (network section) -> http://rozklad.com/maps/index.php?IDKlienta=OSTROW_MZK&cmd=map&mode=map
        directly to the file -> http://rozklad.com/maps/data2json.php?IDKlienta=OSTROW_MZK&co=stopsLatLng

'''

from .static.stops_names import stops_names
from .static.stops_lat_lng import stops_lat_lng

stops_dict = {stop['ID']: stop for stop in stops_lat_lng}

def generate_stops_models_data():
    stops_models = {}

    for stop in stops_names:
        name, id_from_rozklad = stop
        name_cleaned = name.split(" Linie")[0].replace(">>", "").strip()
        if id_from_rozklad in stops_dict:
            latitude = float(stops_dict[id_from_rozklad]['Lat'])
            longitude = float(stops_dict[id_from_rozklad]['Lng'])
        else:
            latitude = None
            longitude = None

        stops_models[id_from_rozklad] = {'name': name_cleaned, 'latitude': latitude, 'longitude': longitude}

    return stops_models
