from django.contrib import admin
from .models import *


admin.site.register(BusLine)
admin.site.register(BusStop)
admin.site.register(BusStopInBusLine)
admin.site.register(BusDeparture)
