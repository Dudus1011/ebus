from django.db import models
from route_search.modules.schedule_web_scrapper.bus_lines import generate_buses_models_data
from route_search.modules.schedule_web_scrapper.bus_stations import generate_stops_models_data
from datetime import datetime
from django.db.models import F, FloatField
from django.db.models.functions import Sqrt, Power



def generate_new_schedule():
    BusLine.objects.all().update(active=False)
    BusStop.add_stops_to_database()
    departures, bus_lines = generate_buses_models_data()
    BusLine.add_lines_to_database(bus_lines)
    BusDeparture.add_departures_to_database(departures)


class BusStop(models.Model):
    '''
    Model for every single busStop, the name can be the same for example in cases where bus stop name is
    from the street name. The difference have to be in latitude and longitude.
    '''
    name = models.CharField(verbose_name='Nazwa przystanku', max_length=128, unique=False)
    latitude = models.FloatField(verbose_name='Szerokość geograficzna', null=True, blank=True)
    longitude = models.FloatField(verbose_name='Długość geograficzna', null=True, blank=True)
    id_from_rozklad = models.CharField(verbose_name='Id ze strony rozklad.com', max_length=128, blank=True, null=True)

    def __str__(self):
        return f'{self.name} - {self.latitude}; {self.longitude}'

    @classmethod
    def add_stops_to_database(cls):
        stops_models_data = generate_stops_models_data()

        for id_from_rozklad, stop_data in stops_models_data.items():
            obj, created = cls.objects.get_or_create(
                id_from_rozklad=id_from_rozklad,
                defaults={
                    'name': stop_data['name'],
                    'latitude': stop_data['latitude'],
                    'longitude': stop_data['longitude']
                }
            )
            if created:
                print(f"Created new stop: {obj.name}")


    @classmethod
    def get_nearest_stops(cls, size_of_response, latitude, longitude):
        distance_expression = Sqrt(Power(F('latitude') - latitude, 2) + Power(F('longitude') - longitude, 2))

        stops = (
            cls.objects
            .annotate(distance=distance_expression)
            .order_by('distance')
            [:size_of_response]
        )
        return stops


    def get_buses_driving_away_from_stop(self, used_buses=None):
        """
        Returns a list of bus lines departing from a specific bus stop.

        :param bus_stop_id: The ID of the bus stop.
        :return: A list of bus lines.
        """
        if used_buses is None:
            used_buses = []
        bus_stop_in_lines = BusStopInBusLine.objects.filter(bus_stop=self).exclude(bus_line__id__in=used_buses)
        driving_away_buses = [bsil.bus_line for bsil in bus_stop_in_lines]
        return driving_away_buses


class BusLine(models.Model):
    '''
    Model to store every single line, for example if there is line 11 and it have more
    than only one route (it can happen for ex. in free days), then we store it in another busLine model
    with the same name, just write it in description which one it is.
    '''
    name = models.CharField(verbose_name='Nazwa linii', max_length=128, unique=False)
    direction = models.CharField(verbose_name='Kierunek', max_length=128, unique=False)
    description = models.TextField(verbose_name='Opis')
    active = models.BooleanField(verbose_name='Aktywny')
    created_at = models.DateTimeField(verbose_name='Data utworzenia', auto_now_add=True)


    def __str__(self):
        return f'{self.name} - {self.direction} - {self.description} - {self.active} - {self.created_at}'

    @classmethod
    def get_or_create_line(cls, line_data):
        return cls.objects.get_or_create(
                name=line_data['name'],
                direction=line_data['direction'],
                active=True,
                description=line_data['description']
            )

    def connect_with_stops(self, stops):
        for stop_id in stops:
            try:
                # Attempt to retrieve the BusStop with the given id_from_rozklad
                bus_stop = BusStop.objects.get(id_from_rozklad=stop_id)
                BusStopInBusLine.objects.create(
                    ordinal_number=stops.index(stop_id) + 1,
                    bus_stop=bus_stop,
                    bus_line=self
                )
            except BusStop.DoesNotExist:
                # Handle the case where the BusStop does not exist
                print(f"BusStop with id_from_rozklad {stop_id} does not exist.")

    @classmethod
    def add_lines_to_database(cls, bus_models_data):
        for line_data in bus_models_data:
            obj, created = cls.get_or_create_line(line_data)
            if created:
                print(f"Created new bus: {obj.name}")
            else:
                '''
                    There is an option that someparts will be the same, so we want to change description to create
                    new bus (for example there is an option that there is a bus with number fe. 22 that have 
                    two directions. Both are Centrum przesiadkowe, after webscrapping we add same description, name and
                    direction, so django will not create this bus. Here we want to change description to add this bus.
                '''
                line_data['description'] += '!!Drugi kierunek!!'
                obj, created = cls.get_or_create_line(line_data)
                if created:
                    print(f"Created new bus: {obj.name}")

            obj.connect_with_stops(line_data['stops'])


class BusStopInBusLine(models.Model):
    '''
    Model to store bus stop information for a bus line.
    '''
    ordinal_number = models.IntegerField(verbose_name='Numer porządkowy w danym odjeździe')
    bus_line = models.ForeignKey(BusLine, on_delete=models.CASCADE)
    bus_stop = models.ForeignKey(BusStop, on_delete=models.CASCADE)

    def __str__(self):
        return f'Bus nr: {self.bus_line.name} - {self. ordinal_number}. {self.bus_stop}'


ROUTE_DAY_CHOICES = (
    ('Roboczy', 'roboczy'),
    ('Sobota', 'sobota'),
    ('Niedziala i święta', 'niedziela i święta')
)


class BusDeparture(models.Model):
    '''
    Model to store data about departures. Departure ordinal number is used to get hours from the same departure.
    For example bus can have to ride from one station to the second one for 10minutes.
    In case when the first station have departure at 10:10 and the second one have departure at 10:12 it is hard to code.
    We would have to use sth like IF TIME + TIME_BETWEEN >= DEPARTURE_SECOND_STOP.
    It can cause problems so I would rather use departure ordinal number and now we will know which ones busDeparture
    objects are connected in one route.
    '''
    departure_ordinal_number = models.IntegerField(verbose_name='Numer porządkowy odjazdu')
    bus_line = models.ForeignKey(BusLine, on_delete=models.CASCADE)
    bus_stop = models.ForeignKey(BusStop, on_delete=models.CASCADE)
    time = models.TimeField(verbose_name="Godzina odjazdu")
    route_day = models.CharField(verbose_name='Dzień odjazdu', max_length=128, choices=ROUTE_DAY_CHOICES)

    def __str__(self):
        return f'{self.bus_line.name} odjazd nr. {self.departure_ordinal_number} - {self.bus_stop.name} o godzinie {self.time}'

    @classmethod
    def add_departures_to_database(cls, departures_data):
        for departure_data in departures_data:
                time_string = departure_data['hour'] + " " + departure_data['minutes']
                timefield = datetime.strptime(time_string, "%H %M").time()
                try:
                    obj, created = cls.objects.get_or_create(
                        departure_ordinal_number=departure_data['departure_ordinal_number'],
                        bus_line=BusLine.objects.get(name=departure_data['bus_line_name'], direction=departure_data['direction'], active=True),
                        bus_stop=BusStop.objects.get(id_from_rozklad=departure_data['bus_stop_id']),
                        time=timefield,
                        route_day=departure_data['day_type']
                    )
                except:
                    created = False
                    print(f'failed: {departure_data["bus_line_name"]} , {departure_data["bus_stop_id"]}')
                if created:
                    print(f"Created new departure: {obj.bus_line}, {obj.bus_stop}, {obj.time}")
